#==========================================
# Copyright © 2020 Leonhart231
# SPDX-License-Identifier: GPL-3.0-or-later
#==========================================

PROJECT=hifive1
ARCH=riscv32-unknown-elf
TYPE=rv32imac
ABI=ilp32
CPP=$(ARCH)-cpp
CC=$(ARCH)-gcc -march=$(TYPE) -mabi=$(ABI)
AS=$(ARCH)-as  -march=$(TYPE) -mabi=$(ABI)
LD=$(ARCH)-ld
OBJCOPY=$(ARCH)-objcopy
OBJDUMP=$(ARCH)-objdump
NM=$(ARCH)-nm
SIZE=$(ARCH)-size

CFLAGS=-std=c11 -pedantic-errors -Wall -Wextra -Werror -O0 -Iinclude \
	-ffreestanding -Wfatal-errors -fno-common

OBJECTS=build/main.o build/crt0.o

.PHONY: all clean

all: $(PROJECT).hex

$(PROJECT).hex: $(OBJECTS)
	$(LD) $< -T fe310-g000.ld --cref -Map $(PROJECT).map -nostartfiles \
		-o $(PROJECT).elf
	$(OBJCOPY) -j .text -j .data -O ihex $(PROJECT).elf $@
	$(OBJDUMP) -D $(PROJECT).elf > $(PROJECT).disasm
	$(NM) -n $(PROJECT).elf > $(PROJECT).sym
	$(SIZE) $(PROJECT).elf > $(PROJECT).size

build/%.o: src/%.s
	mkdir -p build
	$(AS) -o $@ -c $^

build/%.o: src/%.c
	mkdir -p build
	$(CPP) $(CFLAGS) -o $(@D)/$*.i -E $^
	$(CC) $(CFLAGS) -o $(@D)/$*.s -S $(@D)/$*.i
	$(AS) -o $@ -c $(@D)/$*.s

clean:
	rm -r build $(PROJECT)*
