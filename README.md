# HiFive1 Bare-Metal Code
Version 0.1.0

## About
This is bare-metal startup code for the HiFive1 microcontroller made by SiFive. It provides basic startup code to zero out uninitialized global data (.bss), set other global data (.data) to the proper values, configure the global and stack pointers, and call C’s `main()` function. This is for bare-metal programs, meaning that no C standard library or GCC library is linked.

## Choosing the right linker script
By default, this will generate code for the HiFive1 rev A. If you want to compile for the rev B, change the LD command in the [makefile](Makefile) to use the [fe310-g002.ld](fe310-g002.ld) linker script instead of [fe310-g000.ld](fe310-g000.ld).

## Use
You should be able to edit the [main.c](src/main.c) file with your application. Any new source files should go in the `src` folder, and any header files should go in `include`. The `OBJECTS` variable in the [makefile](Makefile) will also have to be updated to compile/assemble/etc. the added files. For example, if you add `src/mycode.c`, you would add `build/mycode.o` to `OBJECTS`.

If you want to program in assembly only, much of the [makefile](Makefile), [linker script](fe310-g000.ld), and [crt0](src/crt0.s) can be deleted (though you can also just define a `main` function in assembly). If you do, the [crt0](src/crt0.s) will then have to be updated to call the proper function, which should also appear as a .extern at the top of the file.

## Warnings
The HiFive1 rev A (which this was written for) starts user code at address 0x20400000. It's possible this will change on other boards. If it does, the linker script will have to be adjusted. Linker scripts are highly device-specific by their nature, so use with caution.
