/*==========================================*\
 * Copyright © 2020 Leonhart231
 * SPDX-License-Identifier: GPL-3.0-or-later
\*==========================================*/

	.globl __start

	.extern __data_end
	.extern __data_lma
	.extern __data_start
	.extern exit
	.extern main
	.extern __global_pointer$
	.extern __init_sp

__start:
/* Global and stack pointer initialization */
.option push
.option norelax
	la gp, __global_pointer$
.option pop
	la sp,  __init_sp

/* .bss zeroing */
	la t0, __bss_start
	la t1, __bss_end
bss_zero_loop:
	sw zero, (t0)
	addi t0, t0, 4
	bltu t0, t1, bss_zero_loop

/* .data initialization */
	la t0, __data_lma
	la t1, __data_start
	la t2, __data_end
	bgeu t1, t2, data_init_end
data_init_loop:
	lw t3, (t0)
	sw t3, (t1)
	addi t0, t0, 4
	addi t1, t1, 4
	bltu t1, t2, data_init_loop
data_init_end:

/* Program execution */
	/* argc = 0 */
	li a0, 0
	/* argv[0] = NULL */
	addi sp, sp, -16
	sw zero, 12(sp)
	addi a1, sp, 12
	call main
	/* a0 has main()’s return value already */
	call exit

/* Infinite loop if exit() returns somehow */
catch_loop:
	j catch_loop
